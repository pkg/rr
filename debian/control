Source: rr
Maintainer: Stephen Kitt <skitt@debian.org>
Section: devel
Priority: optional
Build-Depends: capnproto,
               cmake,
               debhelper-compat (= 13),
               dh-python,
               g++-multilib | g++-i686-linux-gnu,
               gdb,
               libcapnp-dev,
               libdisasm-dev,
               libpfm4-dev,
               pkg-config,
               python3,
               python3-pexpect
# The following are required for the test suite
# realpath
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian/rr
Vcs-Git: https://salsa.debian.org/debian/rr.git
Homepage: http://rr-project.org
Rules-Requires-Root: no

Package: rr
Architecture: amd64 i386
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         sse2-support [i386]
Description: application execution recorder, player and debugger
 rr allows application executions to be recorded and then replayed
 with gdb as many times as desired.
 .
 This allows intermittent, or complex to reproduce, bugs to be
 captured and then debugged at leisure. Replays are deterministic,
 always identical from one run to another.
 .
 rr is incompatible with ptrace hardening, and currently only supports
 Intel CPUs with Nehalem or later microarchitectures, and some Zen
 CPUs. The amd64 package supports debugging amd64 and i386 binaries.
